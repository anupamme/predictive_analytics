import numpy as np
import pandas as pd
import matplotlib.pyplot as plt 
import matplotlib.dates as dates

#idx = pd.date_range('2017-04-01', '2017-10-31')

df_o = pd.read_csv('item_304654_order.csv', encoding='latin1')
date_ind = df_o['d_date']
order_val = df_o['daily_order']
s = pd.Series(order_val.tolist(), index=date_ind)

df_s = pd.read_csv('item_304654_cum_demand.csv', encoding='latin1')
date_ind_d = df_s['d_date']
order_val_d = df_s['FifteenDayTotal']
order_val_l = order_val_d.tolist()
order_val_l = list(map(lambda x: x/3, order_val_l))
d = pd.Series(order_val_l, index=date_ind_d)

fig, ax = plt.subplots()

ax.plot_date(date_ind.tolist(), s, 'v-')
ax.plot_date(date_ind_d.tolist(), d, 'v-')
#ax.xaxis.set_minor_locator(dates.WeekdayLocator(byweekday=(1),
#                                                interval=1))
#ax.xaxis.set_minor_formatter(dates.DateFormatter('%d\n%a'))
#ax.xaxis.grid(True, which="minor")
ax.yaxis.grid()
ax.xaxis.set_major_locator(dates.MonthLocator())
ax.xaxis.set_major_formatter(dates.DateFormatter('\n\n\n%b\n%Y'))


plt.tight_layout()
plt.show()