from pandas import read_csv
from pandas import datetime
from sklearn.metrics import mean_squared_error
from math import sqrt
from matplotlib import pyplot
import pandas as pd

df_s = pd.read_csv('data/item_304654_demand.csv', encoding='latin1')
df_s1=df_s.loc[df_s['daily_order'] < 100000]
df_s2=df_s1.loc[df_s1['daily_order'] < 16000]

rolling_size = 60
s_r = df_s2.rolling(rolling_size).mean()

ax_2 = s_r['daily_order']
ax_1 = df_s2['daily_order']

rmse = sqrt(mean_squared_error(ax_1[155:], ax_2[155:]))
print('rmse_60: ' + str(rmse))

#rolling_size = 7
#while rolling_size < 90:
#    s_r = df_s.rolling(rolling_size).mean()
#    ax_2 = s_r['daily_order']
#    ax_1 = df_s['daily_order']
#    rmse = sqrt(mean_squared_error(ax_1[155:], ax_2[155:]))
#    print('rmse_' + str(rolling_size) + ': ' + str(rmse))
#    rolling_size += 1

pyplot.plot(ax_1)
pyplot.plot(ax_2)
pyplot.show()