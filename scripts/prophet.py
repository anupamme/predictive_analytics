import pandas as pd
import numpy as np
from fbprophet import Prophet
from sklearn.metrics import mean_squared_error
from math import sqrt
from matplotlib import pyplot

forecast_period = 49

df = pd.read_csv('data/item_304654_demand.csv')
df['y'] = np.log(df['daily_order'])
df.columns = ['c_item_code', 'ds', 'daily_order', 'y']
df_m = df[:155]

m = Prophet(weekly_seasonality=False, yearly_seasonality=False)
m.fit(df_m);

future = m.make_future_dataframe(periods=forecast_period)
forecast = m.predict(future)

# calculate rmse
forecast_prediction = forecast.tail(forecast_period)['yhat'].tolist()
forecast_pred_exp = list(map(lambda x: np.exp(x), forecast_prediction))
actuals = df[155:]['daily_order'].tolist()
rmse = sqrt(mean_squared_error(forecast_pred_exp, actuals))    
print(rmse)

m.plot(forecast)
#m.plot_components(forecast)
pyplot.show()